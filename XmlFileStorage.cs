﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace wp_IsolatedStorage
{
	public class XmlFileStorage : IsolatedStorageInterface
	{
		private List<Person> GenerateSamplePersonData()
		{
			var returnValue = new List<Person>();

			returnValue.Add(new Person { FirstName = "Frank", LastName = "Sinatra", Age = 99 });
			returnValue.Add(new Person { FirstName = "Zack", LastName = "Galifanakis", Age = 34 });
			returnValue.Add(new Person { FirstName = "Oberon", LastName = "Obelisk", Age = 245 });

			return returnValue;
		}

		public void WriteXMLObjectToFile()
		{
			var xmlSettings = new XmlWriterSettings();
			xmlSettings.Indent = true;

			using (var userStore = UserStore)
			{
				using (var stream = userStore.OpenFile("people.xml", FileMode.Create))
				{
					var serializer = new XmlSerializer(typeof(List<Person>));

					using (var writer = XmlWriter.Create(stream, xmlSettings))
					{
						serializer.Serialize(writer, GenerateSamplePersonData());
					}
				}
			}
		}

		public void ReadXMLIntoObject()
		{
			try
			{
				using (var userStore = UserStore)
				{
					using (var stream = userStore.OpenFile("people.xml", FileMode.Open))
					{
						var serializer = new XmlSerializer(typeof(List<Person>));

						var data = serializer.Deserialize(stream);
					}
				}
			}
			catch { }
		}
	}

	public class Person
	{
		private string _firstName;

		public string FirstName
		{
			get { return _firstName; }
			set { _firstName = value; }
		}

		private string _lastName;

		public string LastName
		{
			get { return _lastName; }
			set { _lastName = value; }
		}

		private int _age;

		public int Age
		{
			get { return _age; }
			set { _age = value; }
		}

	}
}
