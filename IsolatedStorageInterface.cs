﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wp_IsolatedStorage
{
	public class IsolatedStorageInterface
	{
		private IsolatedStorageFile _userStore;

		public IsolatedStorageFile UserStore
		{
			get
			{
				if (_userStore == null)
				{
					_userStore = IsolatedStorageFile.GetUserStoreForApplication();
				}

				return _userStore;
			}
			set { _userStore = value; }
		}
	}
}
