﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.IsolatedStorage;

namespace wp_IsolatedStorage
{
	class WriteTextFiles
	{
		private IsolatedStorageFile _userStore;

		public IsolatedStorageFile UserStore
		{
			get
			{
				if (_userStore == null)
				{
					_userStore = IsolatedStorageFile.GetUserStoreForApplication();
				}

				return _userStore;
			}
			set { _userStore = value; }
		}

		public void WriteTextToFile()
		{
			using (var writingStream = new StreamWriter(new IsolatedStorageFileStream("myFile.txt", FileMode.Create, FileAccess.Write, UserStore)))
			{
				var data = "some text to be written";

				writingStream.WriteLine(data);
				writingStream.Close();
			}
		}

		public void AppendFile()
		{
			var fileStream = UserStore.OpenFile("myFile.txt", FileMode.Open, FileAccess.Write);

			using (var writingStream = new StreamWriter(fileStream))
			{
				writingStream.WriteLine("extra data for you");
				writingStream.Close();
			}
		}

		public void ReadTextFromFile()
		{
			var fileStream = UserStore.OpenFile("myFile.txt", FileMode.Open, FileAccess.Read);

			var returnText = string.Empty;

			using (var reader = new StreamReader(fileStream))
			{
				returnText = reader.ReadToEnd(); //Or readline, whatev
			}
		}
	}
}
