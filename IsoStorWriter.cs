﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.IsolatedStorage;

namespace wp_IsolatedStorage
{
	public class IsoStorWriter
	{
		private IsolatedStorageFile _userStore;

		public IsolatedStorageFile UserStore
		{
			get
			{
				if (_userStore == null)
				{ _userStore = IsolatedStorageFile.GetUserStoreForApplication(); }

				return _userStore;
			}
			set { _userStore = value; }
		}


		public void CreateFolder(string folderName)
		{
			UserStore.CreateDirectory(folderName);
		}

		public bool DeleteFolder(string folderName, out string message)
		{
			if (!string.IsNullOrWhiteSpace(folderName) && UserStore.DirectoryExists(folderName))
			{
				UserStore.DeleteDirectory(folderName);

				message = string.Empty;
				return true;
			}

			message = "Directory name '" + folderName + "' invalid, or directory does not exist.";
			return false;
		}

		public void WriteNewFile(string fileName)
		{
			//TODO: Write something that parses directory from file and verifies that path exists
			var writeFile = new StreamWriter(new IsolatedStorageFileStream(fileName, FileMode.CreateNew, UserStore));
		}

		public void WriteNewFileAlt(string fileName)
		{
			var writeFile = new IsolatedStorageFileStream(fileName, FileMode.CreateNew, UserStore);
		}

		public void DeleteFile(string fileName)
		{
			UserStore.DeleteFile(fileName);
		}

		public void RemoveEverything()
		{
			UserStore.Remove();
		}
	}
}
