﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wp_IsolatedStorage
{
	public class StorageSettings
	{
		private IsolatedStorageFile _userStore;

		public IsolatedStorageFile UserStore
		{
			get
			{
				if (_userStore == null)
				{ _userStore = IsolatedStorageFile.GetUserStoreForApplication(); }

				return _userStore;
			}
			set { _userStore = value; }
		}

		public void AddOrUpdateSetting(string key, string value)
		{
			var settings = IsolatedStorageSettings.ApplicationSettings;

			if (settings.Contains(key))
			{
				settings[key] = value;
			}
			else
			{
				settings.Add(key, value);
			}
		}

		public object ReturnSetting(string key)
		{
			return IsolatedStorageSettings.ApplicationSettings[key];
		}

		public void SaveComplexObjectToSettings(string key)
		{
			var settings = IsolatedStorageSettings.ApplicationSettings;

			settings.Add(key, new SaveableObject { CountOfSomething = 3, NameOfSomething = "Edmund" });
		}

		public void RetrievComplexObject(string key)
		{
			var settings = IsolatedStorageSettings.ApplicationSettings;

			SaveableObject ourObject;

			settings.TryGetValue<SaveableObject>(key, out ourObject);
		}

		public void DeleteSetting(string key)
		{
			IsolatedStorageSettings.ApplicationSettings.Remove(key);
		}

		public void DeleteAllSettings()
		{
			IsolatedStorageSettings.ApplicationSettings.Clear();
		}
	}

	public class SaveableObject
	{
		private int _countOfSomething;

		public int CountOfSomething
		{
			get { return _countOfSomething; }
			set { _countOfSomething = value; }
		}

		private string _nameOfSomething;

		public string NameOfSomething
		{
			get { return _nameOfSomething; }
			set { _nameOfSomething = value; }
		}


	}
}
